

#include "CtrlParametros.hpp"
#include "FEM2DTriangulos.hpp"
#include "Problema_2DEjemp01.hpp"
#include "Geometria_2DTriangulos.hpp"
#include "ResuelveCGMBandDisp.hpp"
#include "ResuelveJacobiBandDisp.hpp"
#include "ResuelveGaussSeidelBandDisp.hpp"


// Ejemplo para resolver un problema en dos dimensiones mediante el m�todo de elemento finito
int main(int argc, const char *args[])
{
   
   // Control de parametros pasados de la linea de comandos
   CtrlParametros parm(argc, args);
   int nx = parm.intVal("nx",5); // Partici�n en X
   int ny = parm.intVal("ny",nx); // Partici�n en Y


   Geometria_2DTriangulos *ge = new Geometria_2DTriangulos();

   Problema_2DEjemp01 *pr = new Problema_2DEjemp01();
   pr->inicializa(ge,nx,ny);
   pr->visualizaProblema();   
   ge->visualiza();

   
   ResuelveCGMBandDisp *rsl1 = new ResuelveCGMBandDisp();
   ResuelveJacobiBandDisp *rsl2 = new ResuelveJacobiBandDisp();
   ResuelveGaussSeidelBandDisp *rsl3 = new ResuelveGaussSeidelBandDisp();
   
   
   ResuelveSistemaLineal *rsl[3];
   rsl[0] = rsl1;   
   rsl[1] = rsl2;   
   rsl[2] = rsl3;
   
   for (int i = 0; i < 3; i++)
   {
      FEM2DTriangulos fem(pr, ge, rsl[i]);
      if (i == 0) fem.activaVisualiza(true);
       else fem.activaVisualiza(false);
      fem.resuelve();
      fem.visualizaSolucion();
      fem.grabaSolucion("Solucion.txt");
      fem.error();
      printf("\n\n");
   }
   
   
   
   
   delete pr;
   delete ge;
   delete rsl1;
   delete rsl2;
   delete rsl3;
   
   return 0;
}
