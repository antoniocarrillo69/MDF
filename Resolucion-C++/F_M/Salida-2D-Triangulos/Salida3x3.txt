
  Resuelve -(Uxx + Uyy) =  2.0*(1.0 - y*y) + 2.0*(1.0 - x*x)  
  en  -1 < x < 1, -1 < y < 1       
  u(x,0) = 0   u(x,2) = 0          
  u(0,y) = 0   u(1,y) = 0          
  Solucion: (1.0 - x*x)(1.0 - y*y) 


Dominio: (-1.000000,-1.000000) a (1.000000,1.000000)


Nodos de los elementos

Elemento   1 ---> Nodos (   0,  1,  4 )
Elemento   2 ---> Nodos (   1,  5,  4 )
Elemento   3 ---> Nodos (   1,  2,  5 )
Elemento   4 ---> Nodos (   2,  6,  5 )
Elemento   5 ---> Nodos (   2,  3,  6 )
Elemento   6 ---> Nodos (   3,  7,  6 )
Elemento   7 ---> Nodos (   4,  5,  8 )
Elemento   8 ---> Nodos (   5,  9,  8 )
Elemento   9 ---> Nodos (   5,  6,  9 )
Elemento  10 ---> Nodos (   6, 10,  9 )
Elemento  11 ---> Nodos (   6,  7, 10 )
Elemento  12 ---> Nodos (   7, 11, 10 )
Elemento  13 ---> Nodos (   8,  9, 12 )
Elemento  14 ---> Nodos (   9, 13, 12 )
Elemento  15 ---> Nodos (   9, 10, 13 )
Elemento  16 ---> Nodos (  10, 14, 13 )
Elemento  17 ---> Nodos (  10, 11, 14 )
Elemento  18 ---> Nodos (  11, 15, 14 )
Elemento  19 ---> Nodos (  12, 13, 16 )
Elemento  20 ---> Nodos (  13, 17, 16 )
Elemento  21 ---> Nodos (  13, 14, 17 )
Elemento  22 ---> Nodos (  14, 18, 17 )
Elemento  23 ---> Nodos (  14, 15, 18 )
Elemento  24 ---> Nodos (  15, 19, 18 )

Nodos

Nodo:   0 (  -1 ) ---> Nodo: ( -1.0000000000e+00 ,  -1.0000000000e+00  )
Nodo:   1 (  -1 ) ---> Nodo: ( -3.3333333333e-01 ,  -1.0000000000e+00  )
Nodo:   2 (  -1 ) ---> Nodo: ( +3.3333333333e-01 ,  -1.0000000000e+00  )
Nodo:   3 (  -1 ) ---> Nodo: ( +1.0000000000e+00 ,  -1.0000000000e+00  )
Nodo:   4 (  -1 ) ---> Nodo: ( -1.0000000000e+00 ,  -5.0000000000e-01  )
Nodo:   5 (   0 ) ---> Nodo: ( -3.3333333333e-01 ,  -5.0000000000e-01  )
Nodo:   6 (   1 ) ---> Nodo: ( +3.3333333333e-01 ,  -5.0000000000e-01  )
Nodo:   7 (  -1 ) ---> Nodo: ( +1.0000000000e+00 ,  -5.0000000000e-01  )
Nodo:   8 (  -1 ) ---> Nodo: ( -1.0000000000e+00 ,  +0.0000000000e+00  )
Nodo:   9 (   2 ) ---> Nodo: ( -3.3333333333e-01 ,  +0.0000000000e+00  )
Nodo:  10 (   3 ) ---> Nodo: ( +3.3333333333e-01 ,  +0.0000000000e+00  )
Nodo:  11 (  -1 ) ---> Nodo: ( +1.0000000000e+00 ,  +0.0000000000e+00  )
Nodo:  12 (  -1 ) ---> Nodo: ( -1.0000000000e+00 ,  +5.0000000000e-01  )
Nodo:  13 (   4 ) ---> Nodo: ( -3.3333333333e-01 ,  +5.0000000000e-01  )
Nodo:  14 (   5 ) ---> Nodo: ( +3.3333333333e-01 ,  +5.0000000000e-01  )
Nodo:  15 (  -1 ) ---> Nodo: ( +1.0000000000e+00 ,  +5.0000000000e-01  )
Nodo:  16 (  -1 ) ---> Nodo: ( -1.0000000000e+00 ,  +1.0000000000e+00  )
Nodo:  17 (  -1 ) ---> Nodo: ( -3.3333333333e-01 ,  +1.0000000000e+00  )
Nodo:  18 (  -1 ) ---> Nodo: ( +3.3333333333e-01 ,  +1.0000000000e+00  )
Nodo:  19 (  -1 ) ---> Nodo: ( +1.0000000000e+00 ,  +1.0000000000e+00  )


Matriz Densa "Matriz Temporal para Generar Matriz de Rigidez" de dimensi�n: 3 x 3
N�mero de entradas distintas de cero: 7
Numero m�ximo de columnas ocupadas: 3

 +12.0000000000  -12.0000000000  -0.0000000000 
 -12.0000000000  +18.7500000000  -6.7500000000 
 -0.0000000000  -6.7500000000  +6.7500000000 


Matriz Bandada Compacta "Matriz de carga" de dimensi�n: 6 x 6 y banda 7
N�mero de entradas distintas de cero: 24
Numero m�ximo de columnas ocupadas: 5

 +75.0000000000  -18.7500000000  -12.0000000000  +0.0000000000  +0.0000000000  +0.0000000000 
 -18.7500000000  +75.0000000000  -6.7500000000  -12.0000000000  +0.0000000000  +0.0000000000 
 -12.0000000000  -6.7500000000  +75.0000000000  -18.7500000000  -12.0000000000  +0.0000000000 
 +0.0000000000  -12.0000000000  -18.7500000000  +75.0000000000  -6.7500000000  -12.0000000000 
 +0.0000000000  +0.0000000000  -12.0000000000  -6.7500000000  +75.0000000000  -18.7500000000 
 +0.0000000000  +0.0000000000  +0.0000000000  -12.0000000000  -18.7500000000  +75.0000000000 


Vector "Vector de carga" de dimensi�n: 6 

Vector "Vector Extendido" de dimensi�n: 14 

Vector extendido "Vector de carga global" de dimensi�n: 20 
 +1.4012345679e+00 
 +6.4814814815e+00 
 +7.6666666667e+00 
 +3.8950617284e+00 
 +6.4629629630e+00 
 +1.7814814815e+01 
 +1.7814814815e+01 
 +7.7962962963e+00 
 +8.6296296296e+00 
 +2.0814814815e+01 
 +2.0814814815e+01 
 +8.6296296296e+00 
 +7.7962962963e+00 
 +1.7814814815e+01 
 +1.7814814815e+01 
 +6.4629629630e+00 
 +3.8950617284e+00 
 +7.6666666667e+00 
 +6.4814814815e+00 
 +1.4012345679e+00 


M�todo CGM, iteraciones para resolver el sistema lineal 3

Vector "Soluci�n sistema lineal" de dimensi�n: 6 

Vector "Vector Extendido" de dimensi�n: 14 

Vector extendido "Soluci�n" de dimensi�n: 20 
0:  +0.0000000000e+00 
1:  +0.0000000000e+00 
2:  +0.0000000000e+00 
3:  +0.0000000000e+00 
4:  +0.0000000000e+00 
5:  +4.6904139830e-01 
6:  +5.1525495609e-01 
7:  +0.0000000000e+00 
8:  +0.0000000000e+00 
9:  +6.4185496926e-01 
10:  +6.4185496926e-01 
11:  +0.0000000000e+00 
12:  +0.0000000000e+00 
13:  +5.1525495609e-01 
14:  +4.6904139830e-01 
15:  +0.0000000000e+00 
16:  +0.0000000000e+00 
17:  +0.0000000000e+00 
18:  +0.0000000000e+00 
19:  +0.0000000000e+00 



Error por cada nodo
Nodo   0 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   1 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   2 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   3 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   4 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   5 (  0), Soluci�n exacta: +6.6666666667e-01 , Error: +1.9762526836e-01
Nodo   6 (  1), Soluci�n exacta: +6.6666666667e-01 , Error: +1.5141171058e-01
Nodo   7 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   8 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   9 (  2), Soluci�n exacta: +8.8888888889e-01 , Error: +2.4703391962e-01
Nodo  10 (  3), Soluci�n exacta: +8.8888888889e-01 , Error: +2.4703391962e-01
Nodo  11 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  12 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  13 (  4), Soluci�n exacta: +6.6666666667e-01 , Error: +1.5141171058e-01
Nodo  14 (  5), Soluci�n exacta: +6.6666666667e-01 , Error: +1.9762526836e-01
Nodo  15 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  16 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  17 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  18 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  19 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00



M�todo Jacobi, iteraciones para resolver el sistema lineal 18

Vector "Soluci�n sistema lineal" de dimensi�n: 6 

Vector "Vector Extendido" de dimensi�n: 14 

Vector extendido "Soluci�n" de dimensi�n: 20 
0:  +0.0000000000e+00 
1:  +0.0000000000e+00 
2:  +0.0000000000e+00 
3:  +0.0000000000e+00 
4:  +0.0000000000e+00 
5:  +4.6903424469e-01 
6:  +5.1524654119e-01 
7:  +0.0000000000e+00 
8:  +0.0000000000e+00 
9:  +6.4184386321e-01 
10:  +6.4184386321e-01 
11:  +0.0000000000e+00 
12:  +0.0000000000e+00 
13:  +5.1524654119e-01 
14:  +4.6903424469e-01 
15:  +0.0000000000e+00 
16:  +0.0000000000e+00 
17:  +0.0000000000e+00 
18:  +0.0000000000e+00 
19:  +0.0000000000e+00 



Error por cada nodo
Nodo   0 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   1 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   2 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   3 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   4 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   5 (  0), Soluci�n exacta: +6.6666666667e-01 , Error: +1.9763242198e-01
Nodo   6 (  1), Soluci�n exacta: +6.6666666667e-01 , Error: +1.5142012548e-01
Nodo   7 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   8 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   9 (  2), Soluci�n exacta: +8.8888888889e-01 , Error: +2.4704502568e-01
Nodo  10 (  3), Soluci�n exacta: +8.8888888889e-01 , Error: +2.4704502568e-01
Nodo  11 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  12 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  13 (  4), Soluci�n exacta: +6.6666666667e-01 , Error: +1.5142012548e-01
Nodo  14 (  5), Soluci�n exacta: +6.6666666667e-01 , Error: +1.9763242198e-01
Nodo  15 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  16 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  17 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  18 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  19 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00



M�todo Gauss-Seidel, iteraciones para resolver el sistema lineal 11

Vector "Soluci�n sistema lineal" de dimensi�n: 6 

Vector "Vector Extendido" de dimensi�n: 14 

Vector extendido "Soluci�n" de dimensi�n: 20 
0:  +0.0000000000e+00 
1:  +0.0000000000e+00 
2:  +0.0000000000e+00 
3:  +0.0000000000e+00 
4:  +0.0000000000e+00 
5:  +4.6903816285e-01 
6:  +5.1525261462e-01 
7:  +0.0000000000e+00 
8:  +0.0000000000e+00 
9:  +6.4185250438e-01 
10:  +6.4185345515e-01 
11:  +0.0000000000e+00 
12:  +0.0000000000e+00 
13:  +5.1525403541e-01 
14:  +4.6904092587e-01 
15:  +0.0000000000e+00 
16:  +0.0000000000e+00 
17:  +0.0000000000e+00 
18:  +0.0000000000e+00 
19:  +0.0000000000e+00 



Error por cada nodo
Nodo   0 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   1 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   2 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   3 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   4 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   5 (  0), Soluci�n exacta: +6.6666666667e-01 , Error: +1.9762850381e-01
Nodo   6 (  1), Soluci�n exacta: +6.6666666667e-01 , Error: +1.5141405204e-01
Nodo   7 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   8 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo   9 (  2), Soluci�n exacta: +8.8888888889e-01 , Error: +2.4703638451e-01
Nodo  10 (  3), Soluci�n exacta: +8.8888888889e-01 , Error: +2.4703543373e-01
Nodo  11 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  12 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  13 (  4), Soluci�n exacta: +6.6666666667e-01 , Error: +1.5141263126e-01
Nodo  14 (  5), Soluci�n exacta: +6.6666666667e-01 , Error: +1.9762574079e-01
Nodo  15 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  16 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  17 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  18 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00
Nodo  19 ( -1), Soluci�n exacta: +0.0000000000e+00 , Error: +0.0000000000e+00


